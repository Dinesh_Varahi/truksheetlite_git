//
//  MaterialPopupController.m
//  TruckSheet Lite
//
//  Created by HN on 13/10/15.
//  Copyright (c) 2015 Singularity One. All rights reserved.
//

#import "MaterialPopupController.h"

@interface MaterialPopupController ()

@end

@implementation MaterialPopupController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

-(IBAction)closeMaterialPopup:(id)sender
{
    
    UIButton *shiftButtons = (UIButton *)sender;
    NSString *name = shiftButtons.currentTitle;
    NSLog(@"%@",name);
    
    if ([sender isMemberOfClass:[UIButton class]])
    {
        
            [self saveData:(shiftButtons.currentTitle)];
            if (self.delegate && [self.delegate respondsToSelector:@selector(materialButtonClicked:)]) {
                [self.delegate materialButtonClicked:self];
            }
    }
}

-(void)saveData:(NSString*)buttonName
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *materiType = buttonName;
    [defaults setValue:materiType forKey:@"material"];
    
}


@end
