//
//  RelativeLevelPopupController.h
//  TruckSheet Lite
//
//  Created by HN on 13/10/15.
//  Copyright (c) 2015 Singularity One. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol RelativeLevelPoupDelegate;


@interface RelativeLevelPopupController : UIViewController<UITextFieldDelegate>
{
    
    IBOutlet UITextField *txtRelativeLevel;
}

@property(assign,nonatomic) id<RelativeLevelPoupDelegate>delegate;
@end


@protocol RelativeLevelPoupDelegate <NSObject>

-(void)relativeLevelButtonClicked:(RelativeLevelPopupController *)reletiveLevelPopup;

@end