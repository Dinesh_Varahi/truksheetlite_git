//
//  DestinationPopupController.h
//  TruckSheet Lite
//
//  Created by HN on 13/10/15.
//  Copyright (c) 2015 Singularity One. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DestinationPoupDelegate;

@interface DestinationPopupController : UIViewController

@property(assign,nonatomic)id <DestinationPoupDelegate>delegate;
@end


@protocol DestinationPoupDelegate <NSObject>

-(void)destinationButtonClicked:(DestinationPopupController *)destinationPopup;

@end