//
//  RelativeLevelPopupController.m
//  TruckSheet Lite
//
//  Created by HN on 13/10/15.
//  Copyright (c) 2015 Singularity One. All rights reserved.
//

#import "RelativeLevelPopupController.h"

@interface RelativeLevelPopupController ()

@end

@implementation RelativeLevelPopupController

@synthesize delegate;


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    txtRelativeLevel.delegate = self;
    [txtRelativeLevel resignFirstResponder];
    txtRelativeLevel.placeholder =@"Relative Level";
    if (txtRelativeLevel.placeholder != nil) {
        txtRelativeLevel.clearsOnBeginEditing = NO;
            }
    txtRelativeLevel.keyboardType = UIKeyboardTypeNumberPad;
    
}




- (IBAction)closePopup:(id)sender
{
    
    if ([txtRelativeLevel.text isEqualToString:@""]) {
        
        UIAlertView *alertView=[[UIAlertView alloc] initWithTitle:@"Please Enter Relative Level" message:@"" delegate:sender cancelButtonTitle:@"OK" otherButtonTitles: nil ];
        [alertView show];
    }else
    {
        [self saveData:txtRelativeLevel.text];
        if (self.delegate && [self.delegate respondsToSelector:@selector(relativeLevelButtonClicked:)]) {
            [self.delegate relativeLevelButtonClicked:self];
        }
        
    }

    
  
   
    
}

-(void)saveData:(NSString*)relativelevel
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:relativelevel forKey:@"relativeLvl"];
    
}


@end
