//
//  MaterialPopupController.h
//  TruckSheet Lite
//
//  Created by HN on 13/10/15.
//  Copyright (c) 2015 Singularity One. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MaterialPopupDelegate;

@interface MaterialPopupController : UIViewController
@property (assign, nonatomic) id <MaterialPopupDelegate>delegate;

@end

@protocol MaterialPopupDelegate <NSObject>

@optional
-(void)materialButtonClicked:(MaterialPopupController *)materialPopup;

@end