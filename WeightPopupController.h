//
//  WeightPopupController.h
//  TruckSheet Lite
//
//  Created by HN on 13/10/15.
//  Copyright (c) 2015 Singularity One. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol WeightPopupDelegate;

@interface WeightPopupController : UIViewController<UITextFieldDelegate>
{
    
    IBOutlet UITextField *txtWeight;
}
@property (assign,nonatomic) id<WeightPopupDelegate>delegate;

@end

@protocol WeightPopupDelegate <NSObject>

-(void)weightButtonClicked:(WeightPopupController *)weightPopup;

@end