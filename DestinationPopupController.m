//
//  DestinationPopupController.m
//  TruckSheet Lite
//
//  Created by HN on 13/10/15.
//  Copyright (c) 2015 Singularity One. All rights reserved.
//

#import "DestinationPopupController.h"

@interface DestinationPopupController ()

@end

@implementation DestinationPopupController


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (IBAction)closeDestinationPopup:(id)sender
{
    UIButton *destinationButtons = (UIButton *)sender;
    NSString *name = destinationButtons.currentTitle;
    NSLog(@"%@",name);
    
    if ([sender isMemberOfClass:[UIButton class]])
    {
            [self saveData:(destinationButtons.currentTitle)];
            if (self.delegate && [self.delegate respondsToSelector:@selector(destinationButtonClicked:)]) {
                [self.delegate destinationButtonClicked:self];
            }
        
    }
    
}

-(void)saveData:(NSString*)buttonName
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *destination = buttonName;
    [defaults setValue:destination forKey:@"destination"];
    
    
}


@end
