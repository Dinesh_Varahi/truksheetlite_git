//
//  TSMasterDataViewController.m
//  TruckSheet Lite
//
//  Created by Kapsym on 25/09/15.
//  Copyright © 2015 Singularity One. All rights reserved.
//

#import "TSMasterDataViewController.h"
#import "UIViewController+MJPopupViewController.h"
#import "ShiftViewController.h"
#import "TruckDataViewController.h"
#import "TSProductionDataViewController.h"
#import "ViewController.h"


@interface TSMasterDataViewController () <ShiftPopupDelegate,TruckPopupDelegate>{
    UIDatePicker *datePicker;
    
}

@end

@implementation TSMasterDataViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDate *date = [NSDate date];
    NSDateFormatter *dF = [[NSDateFormatter alloc] init];
    [dF setDateFormat:@"dd MMM yyyy"];
    lblDate.text = [NSString stringWithFormat:@"%@",[dF stringFromDate:date]];
    [defaults setObject:lblDate.text forKey:@"date"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)shiftAction:(id)sender {
    ShiftViewController *secondDetailViewController = [[ShiftViewController alloc] initWithNibName:@"ShiftViewController" bundle:nil];
    secondDetailViewController.delegate = self;
    [self presentPopupViewController:secondDetailViewController animationType:MJPopupViewAnimationFade];
    
   }

- (IBAction)truckAction:(id)sender {
    TruckDataViewController *truckView = [[TruckDataViewController alloc] initWithNibName:@"TruckDataViewController" bundle:nil];
    truckView.delegate = self;
    [self presentPopupViewController:truckView animationType:MJPopupViewAnimationFade];
    
}

- (IBAction)nextButtonAction:(id)sender {
    
    if (!([lblShift.text isEqualToString:@"Select Shift"] || [lblTruckType.text isEqualToString: @"Select Truck"]))
    {
        NSString * storyboardName = @"Main";
        NSString * viewControllerID = @"production";
        UIStoryboard * storyboard = [UIStoryboard storyboardWithName:storyboardName bundle:nil];
        TSProductionDataViewController * controller = (TSProductionDataViewController *)[storyboard instantiateViewControllerWithIdentifier:viewControllerID];
        [self presentViewController:controller animated:YES completion:nil];
        
    }
    else
    {
        UIAlertView *alertView=[[UIAlertView alloc] initWithTitle:@"Please Enter Information !!" message:@"" delegate:sender cancelButtonTitle:@"OK" otherButtonTitles: nil ];
        [alertView show];
    }
    
}

- (IBAction)setingButtonAction:(UIButton *)sender {
    
//    [[self presentingViewController] dismissViewControllerAnimated:YES completion:nil];
//    ViewController *settingVC = [[self storyboard] instantiateViewControllerWithIdentifier:@"LoginView"];
//    [self.navigationController pushViewController:settingVC animated:YES];

    
    [self performSegueWithIdentifier:@"SegueToLogin" sender:self];
}


    

- (void)cancelButtonClicked:(ShiftViewController *)ShifDetailController
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    lblShift.text = [NSString stringWithFormat:@"Shift: %@",[defaults valueForKey:@"shift"]];
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
}

- (void)ButtonClicked:(TruckDataViewController *)atruckDetailController
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    lblTruckType.text = [NSString stringWithFormat:@"Truck Type: %@",[defaults valueForKey:@"truck"]];
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
}




@end
