//
//  ViewController.m
//  TruckSheet Lite
//
//  Created by Kapsym on 25/09/15.
//  Copyright © 2015 Singularity One. All rights reserved.
//

#import "ViewController.h"
#import "AppDelegate.h"

@interface ViewController ()
{
    BOOL enableEdit;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    _txtEmail.delegate = self;
    txtCompanyName.delegate = self;
    txtPassword.delegate = self;
    
    if (([[NSUserDefaults standardUserDefaults] objectForKey:@"Company"] != nil && ![[[NSUserDefaults standardUserDefaults] objectForKey:@"Company"]  isEqual: @""]) && ([[NSUserDefaults standardUserDefaults] objectForKey:@"Email"] != nil && ![[[NSUserDefaults standardUserDefaults] objectForKey:@"Email"]  isEqual: @""]) && (([[NSUserDefaults standardUserDefaults] objectForKey:@"Password"] != nil && ![[[NSUserDefaults standardUserDefaults] objectForKey:@"Password"]  isEqual: @""]))  && (([_txtEmail.text  isEqual: @""]) && ([txtCompanyName.text  isEqual: @""]) && ([txtPassword.text  isEqual: @""])))  {
        
        txtCompanyName.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"Company"];
        _txtEmail.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"Email"];
        txtPassword.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"Password"];
    }
    
    if ((![_txtEmail.text isEqualToString:@""])&&(![txtCompanyName.text isEqualToString:@""])&&(![txtPassword.text isEqualToString:@""])) {
        [_txtEmail addTarget:self action:@selector(textField1Active:) forControlEvents:UIControlEventEditingDidBegin];
        [txtCompanyName addTarget:self action:@selector(textField1Active:) forControlEvents:UIControlEventEditingDidBegin];
        [txtPassword addTarget:self action:@selector(textField1Active:) forControlEvents:UIControlEventEditingDidBegin];
    }
    
    
}

- (void)textField1Active:(UITextField *)sender
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Are you sure you wish to change ?" message:@"" delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"No", nil];
    [alert show];
    
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    
        if (buttonIndex == 0)
        {
                // Do something when cancel pressed
        } else {
            [self.view endEditing:YES];
        }
    
}


- (void)saveUserImfo:(NSString *)company second:(NSString *)email third:(NSString *)password
{
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    
    if (standardUserDefaults) {
        [standardUserDefaults setObject:email forKey:@"Email"];
        [standardUserDefaults setObject:company forKey:@"Company"];
        [standardUserDefaults setObject:password forKey:@"Password"];
        [standardUserDefaults synchronize];
    }

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)saveButtonClicked:(id)sender
{
    
    if (((_txtEmail.text != nil) && (![_txtEmail.text isEqualToString:@""])) && ((txtCompanyName != nil)&& (![txtCompanyName.text isEqualToString:@""])) && ((txtPassword != nil)&& (![txtPassword.text isEqualToString:@""]))) {
        
        [self performSegueWithIdentifier:@"SegueToMasterDataScreen" sender:self];
        [[NSUserDefaults standardUserDefaults] setValue:_txtEmail.text forKey:@"email"];
        [self saveUserImfo:txtCompanyName.text second:_txtEmail.text third:txtPassword.text];
    
    }else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please Enter Information" message:@"" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    
}


@end
