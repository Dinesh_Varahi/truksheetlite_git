//
//  TruckDataViewController.h
//  TruckSheet Lite
//
//  Created by HN on 13/10/15.
//  Copyright (c) 2015 Singularity One. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TruckPopupDelegate;

@interface TruckDataViewController : UIViewController
@property (assign, nonatomic) id <TruckPopupDelegate>delegate;
@end

@protocol TruckPopupDelegate<NSObject>
@optional
- (void)ButtonClicked:(TruckDataViewController  *)trucViewController;
@end