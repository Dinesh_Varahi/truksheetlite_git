//
//  ShiftViewController.h
//  TruckSheet Lite
//
//  Created by HN on 08/10/15.
//  Copyright (c) 2015 Singularity One. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ShiftPopupDelegate;


@interface ShiftViewController : UIViewController

@property (assign, nonatomic) id <ShiftPopupDelegate>delegate;

@end



@protocol ShiftPopupDelegate<NSObject>
@optional
- (void)cancelButtonClicked:(ShiftViewController*)secondDetailViewController;
@end