//
//  TSProductionDataViewController.m
//  TruckSheet Lite
//
//  Created by HN on 09/10/15.
//  Copyright (c) 2015 Singularity One. All rights reserved.
//

#import "TSProductionDataViewController.h"
#import "UIViewController+MJPopupViewController.h"
#import <Foundation/Foundation.h>
#import "MaterialPopupController.h"
#import "MaterialPopupController.h"
#import "DestinationPopupController.h"
#import "WeightPopupController.h"
#import "RelativeLevelPopupController.h"
#import "ViewController.h"

#import <QuartzCore/QuartzCore.h>
#import "AppDelegate.h"


@interface TSProductionDataViewController ()<MaterialPopupDelegate,DestinationPoupDelegate,WeightPopupDelegate,RelativeLevelPoupDelegate>
{
    
    
    double progress;
    NSArray *labeledProgressViews;
    BOOL clearLoadTimer;
    BOOL clearHaulTimer;
    BOOL clearReturnTimer;
    BOOL clearAllTimers;
}
@end

@implementation TSProductionDataViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    running = false;
    
    _timerButtonOnOff = 0;
    
    // LOAD TIMER LABEL
    
    self.lblLoadHours = [[DALabeledCircularProgressView alloc] initWithFrame:CGRectMake(260.0f, 145.0f, 60.0f, 60.0f)];
    self.lblLoadMinutes = [[DALabeledCircularProgressView alloc] initWithFrame:CGRectMake(325.0f, 145.0f, 60.0f, 60.0f)];
    self.lblLoadSeconds = [[DALabeledCircularProgressView alloc] initWithFrame:CGRectMake(390.0f, 145.0f, 60.0f, 60.0f)];
    
    self.lblLoadHours.roundedCorners = YES;
    self.lblLoadMinutes.roundedCorners = YES;
    self.lblLoadSeconds.roundedCorners = YES;
    
    self.lblLoadHours.progressTintColor = [UIColor colorWithRed: 51/255.0f green:153/255.0f blue:204/255.0f alpha:1.0];
    self.lblLoadMinutes.progressTintColor = [UIColor colorWithRed: 255.0/255.0f green:0/255.0f blue:153.0/255.0f alpha:1.0];
    self.lblLoadSeconds.progressTintColor = [UIColor colorWithRed: 0/255.0f green:204.0/255.0f blue:153.0/255.0f alpha:1.0];
    
    
    self.lblLoadHours.progressLabel.textColor = [UIColor whiteColor];
    self.lblLoadMinutes.progressLabel.textColor = [UIColor whiteColor];
    self.lblLoadSeconds.progressLabel.textColor = [UIColor whiteColor];
    
    self.lblLoadHours.progressLabel.font = [self.lblLoadHours.progressLabel.font fontWithSize:20];
    self.lblLoadMinutes.progressLabel.font = [self.lblLoadMinutes.progressLabel.font fontWithSize:20];
    self.lblLoadSeconds.progressLabel.font = [self.lblLoadSeconds.progressLabel.font fontWithSize:20];
    
    [self.view addSubview:self.lblLoadHours];
    [self.view addSubview:self.lblLoadMinutes];
    [self.view addSubview:self.lblLoadSeconds];
    
    self.lblLoadHours.progressLabel.text = @"00";
    self.lblLoadMinutes.progressLabel.text = @"00";
    self.lblLoadSeconds.progressLabel.text = @"00";
    
    
    // HAUL TIMER LABEL
    self.lblHaulHours = [[DALabeledCircularProgressView alloc] initWithFrame:CGRectMake(260.0f, 250.0f, 60.0f, 60.0f)];
    self.lblHaulMinutes = [[DALabeledCircularProgressView alloc] initWithFrame:CGRectMake(325.0f, 250.0f, 60.0f, 60.0f)];
    self.lblHaulSeconds = [[DALabeledCircularProgressView alloc] initWithFrame:CGRectMake(390.0f, 250.0f, 60.0f, 60.0f)];
    
    self.lblHaulHours.roundedCorners = YES;
    self.lblHaulMinutes.roundedCorners = YES;
    self.lblHaulSeconds.roundedCorners = YES;
    
    self.lblHaulHours.progressTintColor = [UIColor colorWithRed: 51/255.0f green:153/255.0f blue:204/255.0f alpha:1.0];
    self.lblHaulMinutes.progressTintColor = [UIColor colorWithRed: 255.0/255.0f green:0/255.0f blue:153.0/255.0f alpha:1.0];
    self.lblHaulSeconds.progressTintColor = [UIColor colorWithRed: 0/255.0f green:204.0/255.0f blue:153.0/255.0f alpha:1.0];
    
    
    self.lblHaulHours.progressLabel.textColor = [UIColor whiteColor];
    self.lblHaulMinutes.progressLabel.textColor = [UIColor whiteColor];
    self.lblHaulSeconds.progressLabel.textColor = [UIColor whiteColor];
    
    self.lblHaulHours.progressLabel.font = [self.lblHaulHours.progressLabel.font fontWithSize:20];
    self.lblHaulMinutes.progressLabel.font = [self.lblHaulMinutes.progressLabel.font fontWithSize:20];
    self.lblHaulSeconds.progressLabel.font = [self.lblHaulSeconds.progressLabel.font fontWithSize:20];
    
    [self.view addSubview:self.lblHaulHours];
    [self.view addSubview:self.lblHaulMinutes];
    [self.view addSubview:self.lblHaulSeconds];
    
    self.lblHaulHours.progressLabel.text = @"00";
    self.lblHaulMinutes.progressLabel.text = @"00";
    self.lblHaulSeconds.progressLabel.text = @"00";
    
    // WAIT TIMER LABEL
    self.lblWaitHours = [[DALabeledCircularProgressView alloc] initWithFrame:CGRectMake(260.0f, 358.0f, 60.0f, 60.0f)];
    self.lblWaitMinutes = [[DALabeledCircularProgressView alloc] initWithFrame:CGRectMake(325.0f, 358.0f, 60.0f, 60.0f)];
    self.lblWaitSeconds = [[DALabeledCircularProgressView alloc] initWithFrame:CGRectMake(390.0f, 358.0f, 60.0f, 60.0f)];
    
    self.lblWaitHours.roundedCorners = YES;
    self.lblWaitMinutes.roundedCorners = YES;
    self.lblWaitSeconds.roundedCorners = YES;
    
    self.lblWaitHours.progressTintColor = [UIColor colorWithRed: 51/255.0f green:153/255.0f blue:204/255.0f alpha:1.0];
    self.lblWaitMinutes.progressTintColor = [UIColor colorWithRed: 255.0/255.0f green:0/255.0f blue:153.0/255.0f alpha:1.0];
    self.lblWaitSeconds.progressTintColor = [UIColor colorWithRed: 0/255.0f green:204.0/255.0f blue:153.0/255.0f alpha:1.0];
    
    
    self.lblWaitHours.progressLabel.textColor = [UIColor whiteColor];
    self.lblWaitHours.progressLabel.alpha = 0.2;
    self.lblWaitMinutes.progressLabel.textColor = [UIColor whiteColor];
    self.lblWaitMinutes.progressLabel.alpha = 0.2;
    self.lblWaitSeconds.progressLabel.textColor = [UIColor whiteColor];
    self.lblWaitSeconds.progressLabel.alpha = 0.2;
    
    self.lblWaitHours.progressLabel.font = [self.lblWaitHours.progressLabel.font fontWithSize:20];
    self.lblWaitMinutes.progressLabel.font = [self.lblWaitMinutes.progressLabel.font fontWithSize:20];
    self.lblWaitSeconds.progressLabel.font = [self.lblWaitSeconds.progressLabel.font fontWithSize:20];
    
    [self.view addSubview:self.lblWaitHours];
    [self.view addSubview:self.lblWaitMinutes];
    [self.view addSubview:self.lblWaitSeconds];
    
    self.lblWaitHours.progressLabel.text = @"00";
    self.lblWaitMinutes.progressLabel.text = @"00";
    self.lblWaitSeconds.progressLabel.text = @"00";
    
    
    
    
    // STOP TIMER LABEL
    self.lblStopHours = [[DALabeledCircularProgressView alloc] initWithFrame:CGRectMake(260.0f, 465.0f, 60.0f, 60.0f)];
    self.lblStopMinutes = [[DALabeledCircularProgressView alloc] initWithFrame:CGRectMake(325.0f, 465.0f, 60.0f, 60.0f)];
    self.lblStopSeconds = [[DALabeledCircularProgressView alloc] initWithFrame:CGRectMake(390.0f, 465.0f, 60.0f, 60.0f)];
    
    self.lblStopHours.roundedCorners = YES;
    self.lblStopMinutes.roundedCorners = YES;
    self.lblStopSeconds.roundedCorners = YES;
    
    self.lblStopHours.progressTintColor = [UIColor colorWithRed: 51/255.0f green:153/255.0f blue:204/255.0f alpha:1.0];
    self.lblStopMinutes.progressTintColor = [UIColor colorWithRed: 255.0/255.0f green:0/255.0f blue:153.0/255.0f alpha:1.0];
    self.lblStopSeconds.progressTintColor = [UIColor colorWithRed: 0/255.0f green:204.0/255.0f blue:153.0/255.0f alpha:1.0];
    
    
    self.lblStopHours.progressLabel.textColor = [UIColor whiteColor];
    self.lblStopHours.progressLabel.alpha = 0.2;
    self.lblStopMinutes.progressLabel.textColor = [UIColor whiteColor];
    self.lblStopMinutes.progressLabel.alpha = 0.2;
    self.lblStopSeconds.progressLabel.textColor = [UIColor whiteColor];
    self.lblStopSeconds.progressLabel.alpha = 0.2;
    
    
    self.lblStopHours.progressLabel.font = [self.lblStopHours.progressLabel.font fontWithSize:20];
    self.lblStopMinutes.progressLabel.font = [self.lblStopMinutes.progressLabel.font fontWithSize:20];
    self.lblStopSeconds.progressLabel.font = [self.lblStopSeconds.progressLabel.font fontWithSize:20];
    
    [self.view addSubview:self.lblStopHours];
    [self.view addSubview:self.lblStopMinutes];
    [self.view addSubview:self.lblStopSeconds];
    
    self.lblStopHours.progressLabel.text = @"00";
    self.lblStopMinutes.progressLabel.text = @"00";
    self.lblStopSeconds.progressLabel.text = @"00";
    
    
    // DUMP TIMER LABEL
    self.lblDumpHours = [[DALabeledCircularProgressView alloc] initWithFrame:CGRectMake(260.0f, 575.0f, 60.0f, 60.0f)];
    self.lblDumpMinutes = [[DALabeledCircularProgressView alloc] initWithFrame:CGRectMake(325.0f, 575.0f, 60.0f, 60.0f)];
    self.lblDumpSeconds = [[DALabeledCircularProgressView alloc] initWithFrame:CGRectMake(390.0f, 575.0f, 60.0f, 60.0f)];
    
    self.lblDumpHours.roundedCorners = YES;
    self.lblDumpMinutes.roundedCorners = YES;
    self.lblDumpSeconds.roundedCorners = YES;
    
    self.lblDumpHours.progressTintColor = [UIColor colorWithRed: 51/255.0f green:153/255.0f blue:204/255.0f alpha:1.0];
    self.lblDumpMinutes.progressTintColor = [UIColor colorWithRed: 255.0/255.0f green:0/255.0f blue:153.0/255.0f alpha:1.0];
    self.lblDumpSeconds.progressTintColor = [UIColor colorWithRed: 0/255.0f green:204.0/255.0f blue:153.0/255.0f alpha:1.0];
    
    
    self.lblDumpHours.progressLabel.textColor = [UIColor whiteColor];
    self.lblDumpHours.progressLabel.alpha = 0.2;
    self.lblDumpMinutes.progressLabel.textColor = [UIColor whiteColor];
    self.lblDumpMinutes.progressLabel.alpha = 0.2;
    self.lblDumpSeconds.progressLabel.textColor = [UIColor whiteColor];
    self.lblDumpSeconds.progressLabel.alpha = 0.2;
    
    self.lblDumpHours.progressLabel.font = [self.lblDumpHours.progressLabel.font fontWithSize:20];
    self.lblDumpMinutes.progressLabel.font = [self.lblDumpMinutes.progressLabel.font fontWithSize:20];
    self.lblDumpSeconds.progressLabel.font = [self.lblDumpSeconds.progressLabel.font fontWithSize:20];
    
    [self.view addSubview:self.lblDumpHours];
    [self.view addSubview:self.lblDumpMinutes];
    [self.view addSubview:self.lblDumpSeconds];
    
    self.lblDumpHours.progressLabel.text = @"00";
    self.lblDumpMinutes.progressLabel.text = @"00";
    self.lblDumpSeconds.progressLabel.text = @"00";
    
    
    // RETURN TIMER LABEL
    self.lblReturnHours = [[DALabeledCircularProgressView alloc] initWithFrame:CGRectMake(260.0f, 685.0f, 60.0f, 60.0f)];
    self.lblReturnMinutes = [[DALabeledCircularProgressView alloc] initWithFrame:CGRectMake(325.0f, 685.0f, 60.0f, 60.0f)];
    self.lblReturnSeconds = [[DALabeledCircularProgressView alloc] initWithFrame:CGRectMake(390.0f, 685.0f, 60.0f, 60.0f)];
    
    self.lblReturnHours.roundedCorners = YES;
    self.lblReturnMinutes.roundedCorners = YES;
    self.lblReturnSeconds.roundedCorners = YES;
    
    self.lblReturnHours.progressTintColor = [UIColor colorWithRed: 51/255.0f green:153/255.0f blue:204/255.0f alpha:1.0];
    self.lblReturnMinutes.progressTintColor = [UIColor colorWithRed: 255.0/255.0f green:0/255.0f blue:153.0/255.0f alpha:1.0];
    self.lblReturnSeconds.progressTintColor = [UIColor colorWithRed: 0/255.0f green:204.0/255.0f blue:153.0/255.0f alpha:1.0];
    
    
    self.lblReturnHours.progressLabel.textColor = [UIColor whiteColor];
    self.lblReturnMinutes.progressLabel.textColor = [UIColor whiteColor];
    self.lblReturnSeconds.progressLabel.textColor = [UIColor whiteColor];
    
    self.lblReturnHours.progressLabel.font = [self.lblReturnHours.progressLabel.font fontWithSize:20];
    self.lblReturnMinutes.progressLabel.font = [self.lblReturnMinutes.progressLabel.font fontWithSize:20];
    self.lblReturnSeconds.progressLabel.font = [self.lblReturnSeconds.progressLabel.font fontWithSize:20];
    
    
    [self.view addSubview:self.lblReturnHours];
    [self.view addSubview:self.lblReturnMinutes];
    [self.view addSubview:self.lblReturnSeconds];
    
    self.lblReturnHours.progressLabel.text = @"00";
    self.lblReturnMinutes.progressLabel.text = @"00";
    self.lblReturnSeconds.progressLabel.text = @"00";
    
    
    
    
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    NSLog (@"mail sent");
    [self dismissViewControllerAnimated:YES completion:NULL];
    
    
}


// Timer buttons Action
-(IBAction)buttonTapped:(id)sender {
    
    UIButton *timerButtons = (UIButton *)sender;
    
    
    if ([sender isMemberOfClass:[UIButton class]])
    {
        
        if (!([lblMaterial.text isEqualToString:@"Select material"] || [lblDestination.text isEqualToString: @"Select Mine Location"] || [lblWeight.text isEqualToString: @"0"] ||[ lblRelativeLevel.text isEqualToString: @"0"]))
        {
            //            if([timerButtons.currentTitle isEqualToString:@"Load"])
            if(timerButtons.tag == 1)
            {
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                
                button = timerButtons.tag;
                _noOflLoads++;
                totalLoads.text = [NSString stringWithFormat:@"%02d",_noOflLoads];
                [defaults setValue:totalLoads.text forKey:@"TotalLoads"];
                [self startTime];
                if (clearReturnTimer) {
                    
                    clearLoadTimer = NO;
                    clearReturnTimer = NO;
                    //                    clearAllTimers = YES;
                    [self clearAllTimer];
                    
                }else
                {
                    clearLoadTimer = YES;
                }
                
                
                
                //            }else if([timerButtons.currentTitle isEqualToString:@"Haul"])
            }else if(timerButtons.tag == 2)
            {
                
                button = timerButtons.tag;
                [self startTime];
                if (clearLoadTimer) {
                    clearHaulTimer = YES;
                }
                
                //            }else if([timerButtons.currentTitle isEqualToString:@"Wait"])
            }else if(timerButtons.tag == 3)
            {
                
                button = timerButtons.tag;
                [self startTime];
                //            }else if([timerButtons.currentTitle isEqualToString:@"Stop"])
            }else if(timerButtons.tag == 4)
            {
                
                button = timerButtons.tag;
                [self startTime];
                //            }else if([timerButtons.currentTitle isEqualToString:@"Dump"])
            }else if(timerButtons.tag == 5)
            {
                
                button = timerButtons.tag;
                [self startTime];
                //            }else if([timerButtons.currentTitle isEqualToString:@"Return"])
            }else if(timerButtons.tag == 6)
            {
                
                button = timerButtons.tag;
                [self startTime];
                
                if (clearHaulTimer) {
                    clearReturnTimer = YES;
                }
                
            }
        }
        else
        {
            UIAlertView *alertView=[[UIAlertView alloc] initWithTitle:@"Please enter Right Side Information first !!" message:@"" delegate:sender cancelButtonTitle:@"OK" otherButtonTitles: nil ];
            [alertView show];
        }
    }
    
}
// End Shift button Action
- (IBAction)endShift:(id)sender {
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [self stopTime];
    
    [self saveData];
    
    // [self fetchData];
    
    /*On end of Shift button click TS_DATA.csv file attached to mail and send to the specified mail ID */
    
    NSString *docPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES) objectAtIndex:0];
    NSString *fileName = @"TS_DATA.csv";
    NSString * document = [docPath stringByAppendingPathComponent:fileName];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:document]) {
        NSFileHandle *fileHandle = [NSFileHandle fileHandleForReadingAtPath:document];
        NSString *fileContent = [[NSString alloc] initWithData:[fileHandle availableData] encoding:NSUTF8StringEncoding];
        [fileHandle closeFile];
        
        NSLog(@"%@",fileContent);
    }
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    
    if ([MFMailComposeViewController canSendMail]) {
        
        [mc setTitle:[NSString stringWithFormat:@"Test Mail"]];
        [mc setSubject:[NSString stringWithFormat:@"TruckSheet Data."]];
        [mc setMessageBody:[NSString stringWithFormat:@"TruckSheet Data is attached with mail.."] isHTML:NO];
        
        
        NSArray *toRecipents = [NSArray arrayWithObject:[defaults valueForKey:@"email"]];
        
        [mc addAttachmentData:[NSData dataWithContentsOfFile:document]
                     mimeType:@"text/csv"
                     fileName:fileName];
        [mc setToRecipients:toRecipents];
        [self presentViewController:mc animated:YES completion:NULL];
        
    }
    
}
// Back To Master action
- (IBAction)backToMasterData:(id)sender {
    
    [self performSegueWithIdentifier:@"SegueToMasterScreenFromData" sender:self];
    
}
//**********************************For Clearing all timer cirlces with their values*********************************
- (void)clearAllTimer {
    self.lblLoadHours.progressLabel.text = @"00";
    [self.lblLoadHours setProgress:0.f animated:YES];
    self.lblLoadMinutes.progressLabel.text = @"00";
    [self.lblLoadMinutes setProgress:0.f animated:YES];
    self.lblLoadSeconds.progressLabel.text = @"00";
    [self.lblLoadSeconds setProgress:0.f animated:YES];
    
    self.lblHaulHours.progressLabel.text = @"00";
    [self.lblHaulHours setProgress:0.f animated:YES];
    self.lblHaulMinutes.progressLabel.text = @"00";
    [self.lblHaulMinutes setProgress:0.f animated:YES];
    self.lblHaulSeconds.progressLabel.text = @"00";
    [self.lblHaulSeconds setProgress:0.f animated:YES];
    
    self.lblWaitHours.progressLabel.text = @"00";
    [self.lblWaitHours setProgress:0.f animated:YES];
    self.lblWaitMinutes.progressLabel.text = @"00";
    [self.lblWaitMinutes setProgress:0.f animated:YES];
    self.lblWaitSeconds.progressLabel.text = @"00";
    [self.lblWaitSeconds setProgress:0.f animated:YES];
    
    self.lblStopHours.progressLabel.text = @"00";
    [self.lblStopHours setProgress:0.f animated:YES];
    self.lblStopMinutes.progressLabel.text = @"00";
    [self.lblStopMinutes setProgress:0.f animated:YES];
    self.lblStopSeconds.progressLabel.text = @"00";
    [self.lblStopSeconds setProgress:0.f animated:YES];
    
    self.lblDumpHours.progressLabel.text = @"00";
    [self.lblDumpHours setProgress:0.f animated:YES];
    self.lblDumpMinutes.progressLabel.text = @"00";
    [self.lblDumpMinutes setProgress:0.f animated:YES];
    self.lblDumpSeconds.progressLabel.text = @"00";
    [self.lblDumpSeconds setProgress:0.f animated:YES];
    
    self.lblReturnHours.progressLabel.text = @"00";
    [self.lblReturnHours setProgress:0.f animated:YES];
    self.lblReturnMinutes.progressLabel.text = @"00";
    [self.lblReturnMinutes setProgress:0.f animated:YES];
    self.lblReturnSeconds.progressLabel.text = @"00";
    [self.lblReturnSeconds setProgress:0.f animated:YES];
    
    _loadTimeInSeconds = 0;
    _haulTimeInSeconds = 0;
    _returnTimeInSeconds = 0;
    _currentTimeInSeconds = 0;
    
}





-(void)startTime{
    
    
    
    if (_timerButtonOnOff == 1) {
        [_myTimer invalidate];
        _timerButtonOnOff = 0;
    }
    //    else{
    
    switch (button) {
        case 1:
            _timerButtonOnOff = 1;
            _currentTimeInSeconds = _loadTimeInSeconds ;
            break;
        case 2:
            _timerButtonOnOff = 1;
            _currentTimeInSeconds = _haulTimeInSeconds ;
            break;
        case 6:
            _timerButtonOnOff = 1;
            _currentTimeInSeconds = _returnTimeInSeconds ;
            break;
        default:
            _timerButtonOnOff = 1;
            _currentTimeInSeconds = 0 ;
            break;
    }
    if (!_myTimer) {
        _myTimer = [self createTimer];
        
    }
    //        _timerButtonOnOff = 1;
    //    }
    
    
    
}

-(void)stopTime {
    [_myTimer invalidate];
}

- (NSTimer *)createTimer {
    
    return [NSTimer scheduledTimerWithTimeInterval:1.0
                                            target:self
                                          selector:@selector(timerTicked:)
                                          userInfo:nil
                                           repeats:YES
            ];
}

-(void)timerTicked:(NSTimer *)timer{
    
    _currentTimeInSeconds++;
    
    _costSeconds++;
    Float64 toatlCost = _costSeconds * 0.11;
    lblCost.text =  [NSString stringWithFormat:@"$%.2f",toatlCost];
    
    
    switch (button) {
        case 1:
            _loadTimeInSeconds = _currentTimeInSeconds;
            // loadTime.text = [self formattedTime:_currentTimeInSeconds];
            [self secondsTimerCircle];
            [self minutesTimerCircle];
            [self hoursTimerCircle];
            break;
        case 2:
            _haulTimeInSeconds = _currentTimeInSeconds;
            //            haulTime.text = [self formattedTime:_currentTimeInSeconds];
            [self secondsTimerCircle];
            [self minutesTimerCircle];
            [self hoursTimerCircle];
            
            break;
        case 3:
            //            waitTime.text = [self formattedTime:_currentTimeInSeconds];
            [self secondsTimerCircle];
            [self minutesTimerCircle];
            [self hoursTimerCircle];
            break;
            
        case 4:
            //            stopTime.text = [self formattedTime:_currentTimeInSeconds];
            [self secondsTimerCircle];
            [self minutesTimerCircle];
            [self hoursTimerCircle];
            break;
            
        case 5:
            //            dumpTime.text = [self formattedTime:_currentTimeInSeconds];
            [self secondsTimerCircle];
            [self minutesTimerCircle];
            [self hoursTimerCircle];
            break;
            
        case 6:
            _returnTimeInSeconds = _currentTimeInSeconds;
            //            returnTime.text = [self formattedTime:_currentTimeInSeconds];
            [self secondsTimerCircle];
            [self minutesTimerCircle];
            [self hoursTimerCircle];
            break;
            
            
        default:
            break;
    }
}

- (void)secondsTimerCircle {
    _timer = [self timeFormatted:_currentTimeInSeconds];
    
    labeledProgressViews = [[NSArray alloc] init];
    switch (button) {
        case 1:
            labeledProgressViews = @[self.lblLoadSeconds];
            break;
        case 2:
            labeledProgressViews = @[self.lblHaulSeconds];
            break;
        case 3:
            labeledProgressViews = @[self.lblWaitSeconds];
            break;
        case 4:
            labeledProgressViews = @[self.lblStopSeconds];
            break;
        case 5:
            labeledProgressViews = @[self.lblDumpSeconds];
            break;
        case 6:
            labeledProgressViews = @[self.lblReturnSeconds];
            break;
        default:
            break;
    }
    for (DALabeledCircularProgressView *labeledProgressView in labeledProgressViews)
    {
        CGFloat secondsProgress = ![_myTimer isValid] ? 0 : labeledProgressView.progress + 0.016f;
        [labeledProgressView setProgress:secondsProgress animated:YES];
        
        if (labeledProgressView.progress >= 0.94f && [_myTimer isValid]) {
            [labeledProgressView setProgress:0.f animated:YES];
        }
        
        
        // labeledProgressView.progressLabel.text = [NSString stringWithFormat:@"%.2f", labeledProgressView.progress];
        //labeledProgressView.progressLabel.text = _timer[@"sec"];
        labeledProgressView.progressLabel.text = [NSString stringWithFormat:@"%@",_timer[@"sec"]];
        
    }
    
}
- (void)minutesTimerCircle {
    _timer = [self timeFormatted:_currentTimeInSeconds];
    
    labeledProgressViews = [[NSArray alloc] init];
    switch (button) {
        case 1:
            labeledProgressViews = @[self.lblLoadMinutes];
            break;
        case 2:
            labeledProgressViews = @[self.lblHaulMinutes];
            break;
        case 3:
            labeledProgressViews = @[self.lblWaitMinutes];
            break;
        case 4:
            labeledProgressViews = @[self.lblStopMinutes];
            break;
        case 5:
            labeledProgressViews = @[self.lblDumpMinutes];
            break;
        case 6:
            labeledProgressViews = @[self.lblReturnMinutes];
            break;
        default:
            break;
    }
    for (DALabeledCircularProgressView *labeledProgressView in labeledProgressViews)
    {
        CGFloat minutesProgress = ![_myTimer isValid] ? 0 : labeledProgressView.progress + 0.000277f;
        
        [labeledProgressView setProgress:minutesProgress animated:YES];
        
        if (labeledProgressView.progress >=  0.94f && [_myTimer isValid]) {
            [labeledProgressView setProgress:0.f animated:YES];
        }
        //        labeledProgressView.progressLabel.text = _timer[@"mins"];
        if ([_timer[@"mins"] isEqualToString:@"0"]) {
            labeledProgressView.progressLabel.text = @"00";
        }
        labeledProgressView.progressLabel.text = [NSString stringWithFormat:@"%2@",_timer[@"mins"]];
        
    }
}
- (void)hoursTimerCircle {
    _timer = [self timeFormatted:_currentTimeInSeconds];
    
    labeledProgressViews = [[NSArray alloc] init];
    switch (button) {
        case 1:
            labeledProgressViews = @[self.lblLoadHours];
            break;
        case 2:
            labeledProgressViews = @[self.lblHaulHours];
            break;
        case 3:
            labeledProgressViews = @[self.lblWaitHours];
            break;
        case 4:
            labeledProgressViews = @[self.lblStopHours];
            break;
        case 5:
            labeledProgressViews = @[self.lblDumpHours];
            break;
        case 6:
            labeledProgressViews = @[self.lblReturnHours];
            break;
        default:
            break;
    }
    for (DALabeledCircularProgressView *labeledProgressView in labeledProgressViews)
    {
        CGFloat hoursProgress = ![_myTimer isValid] ? 0 : labeledProgressView.progress + 0.00001157;
        
        [labeledProgressView setProgress:hoursProgress animated:YES];
        
        if (labeledProgressView.progress >=  0.94f && [_myTimer isValid]) {
            [labeledProgressView setProgress:0.f animated:YES];
        }
        
        if ([_timer[@"hrs"] isEqualToString:@"0"]) {
            labeledProgressView.progressLabel.text = @"00";
        }
        //        labeledProgressView.progressLabel.text = _timer[@"hrs"];
        labeledProgressView.progressLabel.text = [NSString stringWithFormat:@"%2@",_timer[@"hrs"]];
    }
    
}
- (NSString *)formattedTime:(int)totalSeconds {
    int seconds = totalSeconds % 60;
    int minutes = (totalSeconds / 60) % 60;
    int hours = totalSeconds / 3600;
    
    return [NSString stringWithFormat:@"%02d:%02d:%02d",hours,minutes,seconds];
}
-(NSDictionary *)timeFormatted:(int)totalSeconds {
    int seconds = totalSeconds % 60;
    int minutes = (totalSeconds / 60) % 60;
    int hours = totalSeconds / 3600;
    
    return @{ @"sec": [NSString stringWithFormat:@"%02d",seconds], @"mins":[NSString stringWithFormat:@"%02d",minutes],@"hrs":[NSString stringWithFormat:@"%02d",hours]};
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1 ) {
        return;
    }
}

-(NSString *)costOfOperation:(int)totalTime {
    Float64 toatlCost = totalTime * 0.11;
    return [NSString stringWithFormat:@"$%.2f",toatlCost];
}

-(NSString *)replaceNullString:(NSString *)stringObj andFlag:(int)flagType {
    if([stringObj isEqualToString:@""] && flagType == 1)
    {
        NSString *stringObj1 = @"-";
        return stringObj1;
    }else if ([stringObj isEqualToString:@""] && flagType == 2)
    {
        NSString *stringObj1 = @"0";
        return stringObj1;
    }
    else
        return stringObj;
}


-(void)saveData {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSString *email = [defaults valueForKey:@"email"];
    email = [self replaceNullString:email andFlag:1];
    NSString *shift = [defaults valueForKey:@"shift"];
    shift = [self replaceNullString:shift andFlag:1];
    NSString *truck = [defaults valueForKey:@"truck"];
    truck = [self replaceNullString:truck andFlag:2];
    NSString *material = [defaults valueForKey:@"material"];
    material   = [self replaceNullString:material andFlag:1];
    NSString *destination = [defaults valueForKey:@"destination"];
    destination = [self replaceNullString:destination andFlag:1];
    NSString *weight = [defaults valueForKey:@"weight"];
    weight = [self replaceNullString:weight andFlag:2];
    NSString *rlativeLvl = [defaults valueForKey:@"relativeLvl"];
    rlativeLvl = [self replaceNullString:rlativeLvl andFlag:2];
    NSString *date = [defaults valueForKey:@"date"];
    date = [self replaceNullString:date andFlag:1];
    NSString *totalLoad = [defaults valueForKey:@"TotalLoads"];
    totalLoad = [self   replaceNullString:totalLoad andFlag:2];
    NSString *noOfTonnesMined = [defaults valueForKey:@"TotalTonnes"];
    noOfTonnesMined = [self replaceNullString:noOfTonnesMined andFlag:2];
    
    
    NSString *loadSecText = self.lblLoadSeconds.progressLabel.text;
    loadSecText = [self replaceNullString:loadSecText andFlag:2];
    NSString *loadMinText = self.lblLoadMinutes.progressLabel.text;
    loadMinText = [self replaceNullString:loadMinText andFlag:2];
    NSString *loadHrsText = self.lblLoadHours.progressLabel.text ;
    loadHrsText = [self replaceNullString:loadHrsText andFlag:2];
    NSString *loadTimeText = [NSString stringWithFormat:@"%@:%@:%@",loadHrsText,loadMinText,loadSecText];
    
    NSString *haulSecText = self.lblHaulSeconds.progressLabel.text;
    haulSecText = [self replaceNullString:haulSecText andFlag:2];
    NSString *haulMinText = self.lblHaulMinutes.progressLabel.text;
    haulMinText = [self replaceNullString:haulMinText andFlag:2];
    NSString *haulHrsText = self.lblHaulHours.progressLabel.text;
    haulHrsText = [self replaceNullString:haulHrsText andFlag:2];
    NSString *haulTimeText = [NSString stringWithFormat:@"%@:%@:%@",haulHrsText,haulMinText,haulSecText];
    
    NSString *returnSecText = self.lblReturnSeconds.progressLabel.text;
    returnSecText = [self replaceNullString:returnSecText andFlag:2];
    NSString *returnMinText = self.lblReturnMinutes.progressLabel.text;
    returnMinText = [self replaceNullString:returnMinText andFlag:2];
    NSString *retunrHrsText = self.lblReturnHours.progressLabel.text;
    retunrHrsText = [self replaceNullString:retunrHrsText andFlag:2];
    NSString *returnTimeText = [NSString stringWithFormat:@"%@:%@:%@",retunrHrsText,returnMinText,returnSecText];
    
    
    
    NSString *fileData = [NSString stringWithFormat:@"Email:%@,Date:%@,ShiftType:%@,TruckType:%@,MaterialType:%@,Destination:%@,Weight:%@,RelativeLevel:%@,LoadTime:%@,HaulTime:%@,ReturnTime:%@,TotalLoads:%@,NoOfTonnes:%@,TotalCost:%@\n",
                          email,
                          date,
                          shift,
                          truck,
                          material,
                          destination,
                          weight,
                          rlativeLvl,
                          loadTimeText,
                          haulTimeText,
                          returnTimeText,
                          totalLoad,
                          noOfTonnesMined,
                          lblCost.text];
    
    NSLog(@"CSV File Data: %@",fileData);
    
    NSString *docPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES) objectAtIndex:0];
    NSString *fileName = @"TS_DATA.csv";
    NSString * document = [docPath stringByAppendingPathComponent:fileName];
    
    NSLog(@"Doc Path:%@",docPath);
    if (![[NSFileManager defaultManager] fileExistsAtPath:document]) {
        [[NSFileManager defaultManager] createFileAtPath:document contents:nil attributes:nil];
        
    }
    
    NSFileHandle *fileHandle = [NSFileHandle fileHandleForWritingAtPath:document];
    [fileHandle seekToEndOfFile];
    [fileHandle writeData:[fileData dataUsingEncoding:NSUTF8StringEncoding]];
    [fileHandle closeFile];
    NSLog(@"File Saved..");
    
    AppDelegate *appdelegate = [[UIApplication sharedApplication] delegate];
    NSEntityDescription *entity = [NSEntityDescription insertNewObjectForEntityForName:@"TSdata" inManagedObjectContext:appdelegate.managedObjectContext];
    
    [entity setValue:email forKey:@"email"];
    [entity setValue:shift forKey:@"shiftType"];
    [entity setValue:truck forKey:@"truckType"];
    [entity setValue:date  forKey:@"date"];
    
    [entity setValue:loadTimeText forKey:@"loadTime"];
    [entity setValue:haulTimeText forKey:@"haulTime"];
    [entity setValue:returnTimeText forKey:@"returnTime"];
    [entity setValue:material forKey:@"materialType"];
    [entity setValue:destination forKey:@"destination"];
    [entity setValue:lblCost.text forKey:@"cost"];
    [entity setValue:weight forKey:@"weight"];
    [entity setValue:rlativeLvl forKey:@"relativeLevel"];
    
    NSError *error;
    bool saved =[appdelegate.managedObjectContext save:&error];
    NSLog(@"Data Saved: %d",saved);
}

-(void)fetchData {
    
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"TSdata" inManagedObjectContext:context];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entity];
    NSError *error;
    NSMutableArray *mutableFetchResults = [[context executeFetchRequest:request error:&error] mutableCopy];
    if (!mutableFetchResults) {
        NSLog(@"Error..");
    }
    NSLog(@"Data: %@",[mutableFetchResults lastObject]);
    
}

- (IBAction)btnmaterialPopup:(id)sender {
    
    MaterialPopupController *materialView = [[MaterialPopupController alloc] initWithNibName:@"MaterialPopupController" bundle:nil];
    materialView.delegate = self;
    [self presentPopupViewController:materialView animationType:MJPopupViewAnimationFade];
}
- (IBAction)btnDestinationPopup:(id)sender {
    
    DestinationPopupController *destinationView = [[DestinationPopupController alloc] initWithNibName:@"DestinationPopupController" bundle:nil];
    destinationView.delegate = self;
    [self presentPopupViewController:destinationView animationType:MJPopupViewAnimationFade];
}

-(IBAction)btnWeightPopup:(id)sender {
    WeightPopupController *weightView = [[WeightPopupController alloc] initWithNibName:@"WeightPopupController" bundle:nil];
    weightView.delegate = self;
    [self presentPopupViewController:weightView animationType:MJPopupViewAnimationFade];
}

- (IBAction)btnRelativeLevel:(id)sender {
    
    RelativeLevelPopupController *relativeLevelView = [[RelativeLevelPopupController alloc] initWithNibName:@"RelativeLevelPopupController" bundle:nil];
    relativeLevelView.delegate = self;
    [self presentPopupViewController:relativeLevelView animationType:MJPopupViewAnimationFade];
}


-(void)materialButtonClicked:(MaterialPopupController *)materialPopup {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    lblMaterial.text = [defaults valueForKey:@"material"];
    
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    
}

-(void)destinationButtonClicked:(DestinationPopupController *)destinationPopup {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    lblDestination.text = [defaults valueForKey:@"destination"];
    
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
}

-(void)weightButtonClicked:(WeightPopupController *)weightPopup {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    lblWeight.text = [defaults valueForKey:@"weight"];
    totalTonnes.text = [NSString stringWithFormat:@"%0d",([totalTonnes.text intValue] + [lblWeight.text intValue])];
    [defaults setValue:totalTonnes.text forKey:@"TotalTonnes"];
    
    
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
}

-(void)relativeLevelButtonClicked:(RelativeLevelPopupController *)reletiveLevelPopup {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    lblRelativeLevel.text = [defaults valueForKey:@"relativeLvl"];
    
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
}

@end
