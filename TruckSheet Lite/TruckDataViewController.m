//
//  TruckDataViewController.m
//  TruckSheet Lite
//
//  Created by HN on 13/10/15.
//  Copyright (c) 2015 Singularity One. All rights reserved.
//

#import "TruckDataViewController.h"
#import "AppDelegate.h"

@interface TruckDataViewController ()

@end

@implementation TruckDataViewController



- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (IBAction)closeTruckPopup:(id)sender
{
    UIButton *truckButtons = (UIButton *)sender;
    NSString *name = truckButtons.currentTitle;
    NSLog(@"%@",name);
    [self saveData:(truckButtons.currentTitle)];
    if (self.delegate && [self.delegate respondsToSelector:@selector(ButtonClicked:)]) {
        [self.delegate ButtonClicked:self];
    }
    
}


-(void)saveData:(NSString*)buttonName
{
    NSString *truckType = buttonName;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:truckType forKey:@"truck"];
}

@end
