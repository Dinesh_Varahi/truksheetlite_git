//
//  TSProductionDataViewController.h
//  TruckSheet Lite
//
//  Created by HN on 09/10/15.
//  Copyright (c) 2015 Singularity One. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "TSMasterDataViewController.h"
#import "DACircularProgress/DACircularProgressView.h"
#import "DALabeledCircularProgressView.h"
#import <Foundation/Foundation.h>




@interface TSProductionDataViewController : UIViewController <MFMailComposeViewControllerDelegate>
{
    IBOutlet UILabel *loadTime;
    IBOutlet UILabel *haulTime;
    IBOutlet UILabel *waitTime;
    IBOutlet UILabel *stopTime;
    IBOutlet UILabel *dumpTime;
    IBOutlet UILabel *returnTime;
    IBOutlet UILabel *lblCost;
    IBOutlet UILabel *lblDestination;
    IBOutlet UILabel *lblBlockID;
    IBOutlet UILabel *lblMaterial;
    IBOutlet UILabel *lblRelativeLevel;
    IBOutlet UILabel *lblWeight;
    
    IBOutlet UIButton *btnRetrun;
    IBOutlet UIButton *btnLoad;
    
    IBOutlet UIButton *btnHaul;
    
    IBOutlet UIButton *btnMaterial;
    
    IBOutlet UIButton *btnDestination;
    
    IBOutlet UIButton *btnWeight;
    
    IBOutlet UIButton *btnRelativeLevel;
    
    IBOutlet UILabel *totalLoads;
    
    IBOutlet UILabel *totalTonnes;
    
    bool running;
    NSInteger button;
    NSTimeInterval startTime;
    
    
}
- (IBAction)endShift:(id)sender;
-(IBAction)buttonTapped:(id)sender;
- (IBAction)backToMasterData:(id)sender;


@property (weak, nonatomic) NSTimer *myTimer;
@property int currentTimeInSeconds;
@property int loadTimeInSeconds;
@property int haulTimeInSeconds;
@property int returnTimeInSeconds;
@property int costSeconds;
@property int noOflLoads;
@property int noOfTonnesMined;
@property NSDictionary *timer;
@property (nonatomic) int timerButtonOnOff;
@property (nonatomic, weak) IBOutlet UIStepper *stepper;
@property (strong, nonatomic) DALabeledCircularProgressView *lblLoadSeconds;
@property (strong, nonatomic) DALabeledCircularProgressView *lblLoadMinutes;
@property (strong, nonatomic) DALabeledCircularProgressView *lblLoadHours;

@property (strong, nonatomic) DALabeledCircularProgressView *lblHaulSeconds;
@property (strong, nonatomic) DALabeledCircularProgressView *lblHaulMinutes;
@property (strong, nonatomic) DALabeledCircularProgressView *lblHaulHours;

@property (strong, nonatomic) DALabeledCircularProgressView *lblWaitSeconds;
@property (strong, nonatomic) DALabeledCircularProgressView *lblWaitMinutes;
@property (strong, nonatomic) DALabeledCircularProgressView *lblWaitHours;

@property (strong, nonatomic) DALabeledCircularProgressView *lblStopSeconds;
@property (strong, nonatomic) DALabeledCircularProgressView *lblStopMinutes;
@property (strong, nonatomic) DALabeledCircularProgressView *lblStopHours;

@property (strong, nonatomic) DALabeledCircularProgressView *lblDumpSeconds;
@property (strong, nonatomic) DALabeledCircularProgressView *lblDumpMinutes;
@property (strong, nonatomic) DALabeledCircularProgressView *lblDumpHours;

@property (strong, nonatomic) DALabeledCircularProgressView *lblReturnSeconds;
@property (strong, nonatomic) DALabeledCircularProgressView *lblReturnMinutes;
@property (strong, nonatomic) DALabeledCircularProgressView *lblReturnHours;
@end
