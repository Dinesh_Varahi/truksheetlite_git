//
//  ViewController.h
//  TruckSheet Lite
//
//  Created by Kapsym on 25/09/15.
//  Copyright © 2015 Singularity One. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UITextFieldDelegate>
{
    
    IBOutlet UITextField *txtCompanyName;
    IBOutlet UITextField *txtPassword;
 
}
@property (strong, nonatomic) IBOutlet UITextField *txtEmail;

- (IBAction)saveButtonClicked:(id)sender;

@end

