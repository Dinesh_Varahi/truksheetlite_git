//
//  TSMasterDataViewController.h
//  TruckSheet Lite
//
//  Created by Kapsym on 25/09/15.
//  Copyright © 2015 Singularity One. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MJPopupBackgroundView.h"





@protocol dataDelegate <NSObject>

-(void)sendDataToProduction:(NSDictionary *) datadictionary;

@end
@interface TSMasterDataViewController : UIViewController
{
    
    IBOutlet UILabel *lblShift;
    IBOutlet UILabel *lblTruckType;
    IBOutlet UILabel *lblDate;
    
}
- (IBAction)shiftAction:(id)sender;
- (IBAction)truckAction:(id)sender;
- (IBAction)nextButtonAction:(id)sender;
- (IBAction)setingButtonAction:(UIButton *)sender;




@property (weak, nonatomic) IBOutlet UIButton *sendData;

@property(nonatomic,weak) id<dataDelegate>delegate;
@end
