

#import "ShiftViewController.h"
#import "AppDelegate.h"

@interface ShiftViewController ()

@end

@implementation ShiftViewController

@synthesize delegate;


- (IBAction)closePopup:(id)sender
{
    UIButton *shiftButtons = (UIButton *)sender;
    NSString *name = shiftButtons.currentTitle;
    NSLog(@"%@",name);
    
    if ([sender isMemberOfClass:[UIButton class]])
    {
            [self saveData:(shiftButtons.currentTitle)];
            if (self.delegate && [self.delegate respondsToSelector:@selector(cancelButtonClicked:)]) {
                [self.delegate cancelButtonClicked:self];
            }
    }

}

-(void)saveData:(NSString*)buttonName
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *shiftName = buttonName;
    [defaults setValue:shiftName forKey:@"shift"];
    
}

@end
