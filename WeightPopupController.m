//
//  WeightPopupController.m
//  TruckSheet Lite
//
//  Created by HN on 13/10/15.
//  Copyright (c) 2015 Singularity One. All rights reserved.
//

#import "WeightPopupController.h"

@interface WeightPopupController ()

@end

@implementation WeightPopupController


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    txtWeight.delegate = self;
    [txtWeight resignFirstResponder];
    txtWeight.placeholder = @"Enter weight";
    if (txtWeight.placeholder != nil) {
        txtWeight.clearsOnBeginEditing = NO;
    }
    txtWeight.keyboardType = UIKeyboardTypeNumberPad;
}

-(IBAction)closeWeightPopup:(id)sender
{
    if ([txtWeight.text isEqualToString:@""]) {
        
        UIAlertView *alertView=[[UIAlertView alloc] initWithTitle:@"Please Enter Weight" message:@"" delegate:sender cancelButtonTitle:@"OK" otherButtonTitles: nil ];
        [alertView show];
    }else
    {
    
            [self saveData:txtWeight.text];
            if (self.delegate && [self.delegate respondsToSelector:@selector(weightButtonClicked:)]) {
                [self.delegate weightButtonClicked:self];
            }
    }
}

-(void)saveData:(NSString*)wght
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *weight = wght;
    [defaults setValue:weight forKey:@"weight"];
    
}

- (BOOL) textField: (UITextField *)theTextField shouldChangeCharactersInRange:(NSRange)range replacementString: (NSString *)string {
    //return yes or no after comparing the characters
    
    // allow backspace
    if (!string.length)
    {
        return YES;
    }
    
    ////for Decimal value start//////This code use use for allowing single decimal value
        if ([theTextField.text rangeOfString:@"."].location == NSNotFound)
        {
            if ([string isEqualToString:@"."]) {
                return YES;
            }
        }
        else
        {
            if ([[theTextField.text substringFromIndex:[theTextField.text rangeOfString:@"."].location] length]>2)   // this allow 2 digit after decimal
            {
                return NO;
            }
        }
    ////for Decimal value End//////This code use use for allowing single decimal value
    
    // allow digit 0 to 9
    if ([string intValue] || [string isEqualToString:@"0"])
    {
        return YES;
    }
    
    return NO;
}



@end
